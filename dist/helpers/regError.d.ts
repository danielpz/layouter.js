declare const regError: (name: string, message: string, Node?: HTMLElement | Element | undefined) => Error;
export default regError;
