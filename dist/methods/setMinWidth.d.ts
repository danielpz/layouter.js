declare const setMinWidth: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setMinWidth;
