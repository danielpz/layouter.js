declare const setHeight: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setHeight;
