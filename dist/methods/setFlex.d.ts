declare const setFlex: (Node: HTMLElement | Element, flexValues?: string | undefined) => Promise<void | Error>;
export default setFlex;
