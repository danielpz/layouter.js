declare const setRight: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setRight;
