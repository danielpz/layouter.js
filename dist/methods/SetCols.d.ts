declare const setCols: (Node: HTMLElement | Element, columns?: string | undefined) => Promise<void | Error>;
export default setCols;
