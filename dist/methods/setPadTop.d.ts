declare const setPadTop: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setPadTop;
