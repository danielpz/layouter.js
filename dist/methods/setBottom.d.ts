declare const setBottom: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setBottom;
