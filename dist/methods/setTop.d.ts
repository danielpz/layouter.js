declare const setTop: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setTop;
