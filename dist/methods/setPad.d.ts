declare const setPad: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setPad;
