declare const setPosition: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setPosition;
