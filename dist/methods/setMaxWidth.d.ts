declare const setMaxWidth: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setMaxWidth;
