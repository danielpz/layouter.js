declare const setPadRight: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setPadRight;
