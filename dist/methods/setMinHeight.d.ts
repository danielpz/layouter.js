declare const setMinHeight: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setMinHeight;
