declare const setPadLeft: (Node: HTMLElement | Element, values?: string | undefined) => Promise<void | Error>;
export default setPadLeft;
