declare const addClasses: (Node: HTMLElement | Element, classesNames: string, overwrite?: boolean | undefined) => Promise<void>;
export default addClasses;
